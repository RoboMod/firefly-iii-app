FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

# Many lines are copied from https://github.com/firefly-iii/firefly-iii/blob/master/Dockerfile

ENV FIREFLY_VERSION 4.7.8
ENV FIREFLY_PATH /app/code
ENV COMPOSER_ALLOW_SUPERUSER 1

LABEL version="1.0" maintainer="robomod@xeac.org"

## Prepare third parties
# Install packages
RUN apt-get update -y && \
    apt-get install -y --no-install-recommends php-bcmath \
					       cron \
                                               rsyslog \
                                               locales && \
                                               apt-get clean && \
					       rm -rf /var/lib/apt/lists/*

## Configure system
# Generate locales supported by Firefly III
RUN echo "en_US.UTF-8 UTF-8\nde_DE.UTF-8 UTF-8\nfr_FR.UTF-8 UTF-8\nit_IT.UTF-8 UTF-8\nnl_NL.UTF-8 UTF-8\npl_PL.UTF-8 UTF-8\npt_BR.UTF-8 UTF-8\nru_RU.UTF-8 UTF-8\ntr_TR.UTF-8 UTF-8\n\n" > /etc/locale.gen && locale-gen

# Supervisor conf
ADD supervisor/ /etc/supervisor/conf.d/

# Enable apache mod rewrite and ssl
RUN a2enmod rewrite
RUN a2enmod ssl

# Firefly apache conf
ADD apache-firefly.conf /etc/apache2/sites-available/000-default.conf

# Start script
ADD start.sh /app/code/start.sh

## Install software
# Create app code directory
RUN mkdir -p /app/code
WORKDIR /app/code

# Get Firefly
RUN wget "https://github.com/firefly-iii/firefly-iii/archive/${FIREFLY_VERSION}.tar.gz" -O - \
    | tar -xz -C /app/code --strip-components=1

# Run composer
RUN composer install --prefer-dist --no-dev --no-scripts --no-suggest
RUN composer dump-autoload -o

# Setup environment
RUN cat .env.docker | envsubst > .env

# Set owner
RUN chown -R www-data:www-data /app/code

# Expose port
EXPOSE 3000

# Run start script
CMD [ "/app/code/start.sh" ]
